from fastapi import FastAPI
import psycopg
from pydantic import BaseModel

app = FastAPI()


# @app.get("/")
# def root():
#     return {"message": "Hello World"}

# @app.get("/api/categories")
# def get_data():
#     conn_str = "postgresql://trivia-game:trivia-game@db/trivia-game"
#     # Connect to an existing database
#     with psycopg.connect(conn_str) as conn:

#         # Open a cursor to perform database operations
#         with conn.cursor() as cur:
#             # Execute a command: this creates a new table
#             cur.execute("""
#                 SELECT id, title, canon
#                 FROM categories
#                 """)
#             return cur.fetchall()


# @app.get("/api/categories")
# def list_data(page: int=0):
#     conn_str = "postgresql://trivia-game:trivia-game@db/trivia-game"
#     with psycopg.connect(conn_str) as conn:
#         with conn.cursor() as cur:
#             cur.execute("""
#             SELECT id, title, canon
#             FROM categories
#             LIMIT 100 OFFSET %s
#             """,[page*100])
#             records = cur.fetchall()
#             results = []
#             for record in records:
#                 result = {
#                     "id": record[0],
#                     "title": record[1],
#                     "canon": record[2],
#                 }
#                 results.append(result)
#             return results

# @app.get("/api/categories/{category_id}")
# def list_data(category_id: int):
#     conn_str = "postgresql://trivia-game:trivia-game@db/trivia-game"
#     with psycopg.connect(conn_str) as conn:
#         with conn.cursor() as cur:
#             cur.execute("""
#             SELECT id, title, canon
#             FROM categories
#             WHERE id = %s
#             """,[category_id])
#             records = cur.fetchall()
#             results = []
#             for record in records:
#                 result = {
#                     "id": record[0],
#                     "title": record[1],
#                     "canon": record[2],
#                 }
#                 results.append(result)
#             return results

class Category(BaseModel):
    title: str
    canon: bool

@app.post("/api/categories")
def create_data(category: Category):
    print(category)
    return "YAY!"